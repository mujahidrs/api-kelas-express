const express = require("express");
const app = express();
const port = 5000;
const bodyParser = require("body-parser");
const mysql = require("mysql");
const connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'codepolitan_kw'
});

connection.connect();

app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

app.get('/', (request, response)=>{
	response.send("<h1>Hello World</h1>");
});

app.get('/about', (request, response)=>{
	response.send("<h1>About</h1>");
});


//index
app.get('/api/kelas', (request, response)=>{
	let sql = "SELECT * FROM `kelas`";
	let sql2 = "SELECT * FROM `modul` WHERE id_kelas=?";
	let sql3 = "SELECT * FROM `order`, `siswa` WHERE `order`.id_siswa = `siswa`.id AND id_kelas=?";

	connection.query(sql, (err, data_kelas, fields)=>{
		if(!err){
			data_kelas.forEach((kelas, index)=>{
				kelas.data_modul = [];
				kelas.data_order = [];

				connection.query(sql2, kelas.id, (err2, rows2, fields2)=>{
					// console.log({rows});
					// console.log({rows2});

					kelas.data_modul = rows2;

					connection.query(sql3, kelas.id, (err3, rows3, fields3)=>{
						// console.log({rows});
						// console.log({rows2});

						console.log(rows3.length);

						if(rows3.length > 0){
							kelas.data_order = rows3;
							if(!err3){
								response.json({
									status: 200,
									data: data_kelas,
									message: "Berhasil mengambil data kelas"
								});
							}
							else{
								console.log(err2);
							}
						}
					});
				});
			})
		}
		else{
			console.log(err);
		}
	});
});

//store
app.post('/api/users', (request, response)=>{
	let sql = "INSERT INTO siswa SET ?";
	let sql2 = "SELECT * FROM siswa";

	connection.query(sql, request.body, (err, rows, fields)=>{
		if(!err){
			connection.query(sql2, (err2, rows2, fields2)=>{
				if(!err2){
					response.json({
						status: 200,
						data: rows2,
						message: "Berhasil menambah data users"
					});
				}
				else{
					console.log(err2);
				}
			});
		}
		else{
			console.log(err);
		}
	})
	
});

//update
app.put('/api/users/:id', (request, response)=>{
	let sql = `UPDATE siswa SET ? WHERE id=${request.params.id}`;
	let sql2 = "SELECT * FROM siswa";

	connection.query(sql, request.body, (err, rows, fields)=>{
		if(!err){
			connection.query(sql2, (err2, rows2, fields2)=>{
				if(!err2){
					response.json({
						status: 200,
						data: rows2,
						message: `Berhasil mengubah data users id:${request.params.id}`
					});
				}
				else{
					console.log(err2);
				}
			});
		}
		else{
			console.log(err);
		}
	})
});

//destroy
app.delete('/api/users/:id', (request, response)=>{
	let sql = `DELETE FROM siswa WHERE id=${request.params.id}`;
	let sql2 = "SELECT * FROM siswa";

	connection.query(sql, (err, rows, fields)=>{
		if(!err){
			connection.query(sql2, (err2, rows2, fields2)=>{
				if(!err2){
					response.json({
						status: 200,
						data: rows2,
						message: `Berhasil menghapus data users id:${request.params.id}`
					})
				}
				else{
					console.log(err2);
				}
			});
		}
		else{
			console.log(err);
		}
	})
});

app.listen(port,()=>{
	console.log(`Running server on localhost:${port}`);
})